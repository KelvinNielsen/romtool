﻿using System;
using System.Collections.Generic;
using System.IO;

namespace RemoveUpTo
{
    class FileHelper
    {
        private string path;
        public FileHelper(string path)
        {
            this.path = path;
        }
        public void RemoveUpTo(string upTo)
        {
            int i;
            string newPath = "";
            string fileName = "";
            string fullPath = "";
            int postFix = 0;
            int lengthToFiletype = 0;
            int fullLength = 0;
            string[] fileList;

            fileList = Directory.GetFiles(path);

            foreach (string file in fileList)
            {
                i = Path.GetFileName(file).IndexOf(upTo) + upTo.Length;
                fileName = Path.GetFileName(file)[i..];

                newPath = Path.GetDirectoryName(file);
                fullPath = Path.Combine(newPath, fileName);

                lengthToFiletype = fileName.LastIndexOf('.');
                fullLength = fileName.Length;


                while (File.Exists(fullPath))
                {
                    fullPath = Path.Combine(newPath,fileName[..(lengthToFiletype-1)] + "_" + postFix + fileName[lengthToFiletype..]);
                    postFix++;
                }
                Console.WriteLine(string.Format("Renaming\n\t{0} to\n\t{1}.", file, fullPath));

                File.Move(file, fullPath);
            }
        }
        public void MoveAlphabetically()
        {
            UniqueCharList charList = new UniqueCharList();
            char dirName;
            string combinedPath;
            foreach (string file in Directory.EnumerateFiles(path))
            {
                dirName = Path.GetFileName(file)[0];
                
                Directory.CreateDirectory(Path.Combine(path, "" + dirName));

                combinedPath = Path.Combine(path, Path.Combine("" + Path.GetFileName(file)[0],Path.GetFileName(file)));
                File.Move(file, combinedPath);
            }
        }
    }
}
