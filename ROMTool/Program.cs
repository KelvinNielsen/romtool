﻿using System;
using System.IO;

namespace RemoveUpTo
{
    class Program
    {
        static void Main(string[] args)
        {
            string argUpTo = "";
            string workingPath = "";
            try
            {
                if (args[0] == "--RemoveUpTo" || args[0] == "-R")
                {
                    if (args.Length == 2)
                    {
                        workingPath = System.Reflection.Assembly.GetExecutingAssembly().Location;
                        workingPath = Path.GetDirectoryName(workingPath);
                        argUpTo = args[1];
                    }
                    else if (args.Length == 3)
                    {
                        workingPath = args[2];
                        argUpTo = args[1];
                    }
                    FileHelper fileHelper = new FileHelper(workingPath);
                    fileHelper.RemoveUpTo(argUpTo);
                } else if (args[0] == "--MoveAlphabeticall" || args[0] == "-M")
                {
                    if (args.Length == 1)
                    {
                        workingPath = System.Reflection.Assembly.GetExecutingAssembly().Location;
                        workingPath = Path.GetDirectoryName(workingPath);
                    }
                    else if (args.Length == 2)
                    {
                        workingPath = args[1];
                    }
                    FileHelper fileHelper = new FileHelper(workingPath);
                    fileHelper.MoveAlphabetically();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
    }
}

