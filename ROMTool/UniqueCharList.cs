﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace RemoveUpTo
{
    class UniqueCharList:List<char>
    {
        public UniqueCharList() : base()
        {
        }
        public new void Add(char newChar)
        {
            if (!this.Contains(newChar))
                base.Add(newChar);
        }
    }
}
